import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/** Created by roy on 4/2/16. */
final class ControlsPropagator {
    private static StringProperty logData = new SimpleStringProperty("");
    private static BooleanProperty subFolder = new SimpleBooleanProperty();
    private static StringProperty download = new SimpleStringProperty("Downloaded: ");
    private static double byteCount = 0.0;
    private static final Object textBoxLock = new Object();
    private static final Object byteCountLock = new Object();

    // methods that set/format logData based on changes from your UI
    // provide public access to the property
    static StringProperty logDataProperty() { return logData; }

    static void setLogData(String data){
        synchronized (textBoxLock) {
            Platform.runLater(() -> {
                try {
                            logData.set(URLDecoder.decode(data, "UTF-8") + "\n" + logData.get());
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
            );
        }
    }

    static BooleanProperty subFolderProperty() { return subFolder;}

    static Boolean getSubFolder() { return subFolder.get();}

    static StringProperty downloadProperty() {return download;}

    static void setDownload(double downloaded){
        synchronized (byteCountLock){
            byteCount += downloaded;
            String newval = String.format("Downloaded: %.2fMB", byteCount/1048576.0);
            Platform.runLater(() -> download.set(newval));

        }
    }
}
