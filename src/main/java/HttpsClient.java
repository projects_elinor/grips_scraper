/** Created by Roy on 24/05/15.
 *  * modified by elinor to work for UR grips system on 4/7/18.
 */


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

class HttpsClient {
    private final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36";
    private final String HOST = "elearning.uni-regensburg.de";
    private List<String> cookies;
    private HttpsURLConnection conn;

    synchronized String sendPost(String url, String postParams) throws Exception {

        URL obj = new URL(url);
        conn = (HttpsURLConnection) obj.openConnection();

        // Acts like a browser
        conn.setUseCaches(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Host", HOST);
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9,he;q=0.8,de;q=0.7");
        for (String cookie : this.cookies) {
            conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
        }
        conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Content-Language", "en");
        conn.setRequestProperty("Accept-Encoding", " gzip, deflate, br");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", Integer.toString(postParams.length()));

        conn.setDoOutput(true);
        conn.setDoInput(true);

        //System.out.print(postParams);
        // Send post request
        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(postParams);
        wr.flush();
        wr.close();


        BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String inputLine;

        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        //System.out.print(response.toString());

        return response.toString();

    }

    synchronized String GetPageContent(String url) throws Exception {

        URL obj = new URL(null, url, new sun.net.www.protocol.https.Handler());
        conn = (HttpsURLConnection) obj.openConnection();

        // default is GET
        conn.setRequestMethod("GET");

        conn.setUseCaches(false);

        // act like a browser
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9,he;q=0.8,de;q=0.7");
        if (cookies != null) {
            for (String cookie : this.cookies) {
                conn.addRequestProperty("Cookie", cookie.split("; ", 1)[0]);
            }
        }
        BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // Get the response cookies
        setCookies(conn.getHeaderFields().get("Set-Cookie"));

        return response.toString();

    }
    private HttpsURLConnection makeConnection(String url){
        HttpsURLConnection conn = null;
        try {
            URL obj = new URL(null, url, new sun.net.www.protocol.https.Handler());
            conn = (HttpsURLConnection) obj.openConnection();
            conn.setRequestMethod("GET");
            conn.setUseCaches(false);

            // act like a browser
            conn.setRequestProperty("User-Agent", USER_AGENT);
            conn.setRequestProperty("Accept",
                    "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            if (cookies != null) {
                for (String cookie : this.cookies) {
                    conn.addRequestProperty("Cookie", cookie.split("; ", 1)[0]);
                }
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return conn;
    }
    URL getRedirect(String url) {
        HttpsURLConnection conn = makeConnection(url);
        try {
            conn.setInstanceFollowRedirects(false);
            int response =conn.getResponseCode();
            if (response==303){
                String new_link= conn.getHeaderField("Location");
                return getRedirect(new_link);
            }

        }
         catch (IOException e){
                e.printStackTrace();
         }
        return conn.getURL();
    }



    String GetContent(String url){
        HttpsURLConnection conn = makeConnection(url);

        StringBuilder response = new StringBuilder();
        try {
            BufferedReader in =
                    new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

                in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    String getFormParams(String html, String username, String password)
            throws UnsupportedEncodingException {

        Document doc = Jsoup.parse(html);

        Element loginForm = doc.getElementById("login");
        //Elements selectElements = loginForm.getElementsByTag("select");
        Elements inputElements = loginForm.getElementsByTag("input");
        List<String> paramList = new ArrayList<>();
              paramList.add("realm=" + URLEncoder.encode("ur", "UTF-8"));
        for (Element inputElement : inputElements) {
            String key = inputElement.attr("name");
            String value = inputElement.attr("value");

            if (key.equals("username")) {
                value = username;
                paramList.add(key + "=" + URLEncoder.encode(value, "UTF-8"));
            }
            else if (key.equals("password")) {
                value = password;
                paramList.add(key + "=" + URLEncoder.encode(value, "UTF-8"));

            }


        }

        // build parameters list
        StringBuilder result = new StringBuilder();
        for (String param : paramList) {
            if (result.length() == 0) {
                result.append(param);
            } else {
                result.append("&").append(param);
            }
        }

        return result.toString();
    }
    
    private void setCookies(List<String> cookies) {
        this.cookies = cookies;
    }
}